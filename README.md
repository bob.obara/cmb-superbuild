<p style="text-align:center"><img src="images/CMBLogoSuperBuild.png" alt="CMB SuperBuild Logo" align="middle" style="width: 200px;"/> </p>

# CMB SuperBuild

Though CMB is relatively simple to build itself, it does depend on a several libraries including Qt, ParaView/VTK, MOAB, etc..  To help make the process of building CMB and the libraries it depends on easier we have create a SuperBuild CMake Project.
# What You Need To Start

* CMake version 3.5 or greater
* ninja or make - note that for Windows build you will need to have ninja installed
* Checkout of the [CMB SuperBuild Git Repo](https://gitlab.kitware.com/cmb/cmb-superbuild)
* C++ Compiler
 * XCode 7.1 or greater
 * GCC 4.8 
 * Visual Studio 2013 64 bit

Note that the build process will also download additional tar balls and checkout additional git repos so you will also need an internet connection.

# Building CMB using the SuperBuild Process
## Prepping the Git Repo

1. Clone the CMB SuperBuild Repo using git clone https://gitlab.kitware.com/cmb/cmb-superbuild.git
2. cd into the git repo and do the following commands:
 * git submodule init
 * git submodule update

## Configuring the Build Using CMake
There are two possible methods you can use: CMake GUI or the ccmake command line tool
### Using the CMake GUI
![](images/CmakeScreenShot.png)

1. Select the Source directory that contains the CMake SuperBuild Git Repo
2. Select the build directory to be used to hold the build.  Note that due to a bug in git this should not be under and git repository including the Source Directory.

### Using ccmake commandline tool
1. Make the directory you want to build in 
2. cd into that directory
3. If you are building with ninja (as oppose to make) run ccmake -G Ninja PathToYourSuperBuildRepo, else omit the -G Ninja

### Configuring the CMB SuperBuild
* By default the build will be in Release Mode (not Debug) - this can be changed using an advance option
* The process will also build Qt 4.8.6 by default.  If you already have a Qt installed (4.8.*) then you can do the following:
 * Turn USE\_SYSTEM\_qt4 on
 * Tell CMake to configure
 * Check to see if the QT\_QMAKE\_EXECUTABLE variable is set to the appropriate qmake - if is not then set it correctly
 * NOTE!  Qt built on Mac OSX 10.9 and greater needs several patches to work properly.  These patches are automatically applied when Qt is built using the superbuild and it is the recommended way to build Qt.  You will also need to make sure you have a 10.9 SDK on your OSX platform
* tell CMake to configure
* tell CMake to generate

## Building the CMB SuperBuild
* cd into the build directory
* run make or ninja - depending on which build system you previously selected.

## Building a CMB Installable Package
* cd into the build directory
* run ctest -R cpack