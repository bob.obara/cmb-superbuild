include("${CMAKE_CURRENT_LIST_DIR}/../kml.cmake")

superbuild_apply_patch(kml add-missing-unistd.h
  "file_posix.cc misses including unistd.h")
