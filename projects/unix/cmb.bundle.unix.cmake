set(plugins)
foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  superbuild_unix_install_program("${executable}"
    "paraview-${paraview_version};cmb-${cmb_version}")
  list(APPEND plugins
    ${cmb_plugins_${executable}})
endforeach ()

list(REMOVE_DUPLICATES plugins)

foreach (plugin IN LISTS plugins)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "cmb-${cmb_version}"
    ";paraview-${paraview_version};cmb-${cmb_version}"
    "cmb-${cmb_version}/plugins/")
endforeach ()

set(python_modules)
if (pythongirderclient_enabled)
  list(APPEND python_modules
    requests
    girder_client)
endif ()

superbuild_unix_install_python(
  "${CMAKE_INSTALL_PREFIX}"
  "cmb-${cmb_version}"
  MODULES smtk
          shiboken
          paraview
          pygments
          six
          vtk
          ${python_modules}
  MODULE_DIRECTORIES
          "${superbuild_install_location}/lib/python2.7/site-packages"
          "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES
          "cmb-${cmb_version}"
          "paraview-${paraview_version}")

superbuild_unix_install_python(
  "${CMAKE_INSTALL_PREFIX}"
  "cmb-${cmb_version}"
  MODULES vtk
  NAMESPACE paraview
  MODULE_DIRECTORIES
          "${superbuild_install_location}/lib/python2.7/site-packages"
          "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES
          "paraview-${paraview_version}")

if (cmb_install_paraview_python)
  superbuild_unix_install_python(
    "${CMAKE_INSTALL_PREFIX}"
    "paraview-${paraview_version}"
    MODULES paraview
            vtk
    MODULE_DIRECTORIES
            "${superbuild_install_location}/lib/python2.7/site-packages"
            "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES
            "paraview-${paraview_version}")

  superbuild_unix_install_python(
    "${CMAKE_INSTALL_PREFIX}"
    "paraview-${paraview_version}"
    MODULES vtk
    NAMESPACE paraview
    MODULE_DIRECTORIES
            "${superbuild_install_location}/lib/python2.7/site-packages"
            "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES
            "paraview-${paraview_version}")
endif ()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/python2.7/site-packages/requests/cacert.pem"
    DESTINATION "lib/python2.7/site-packages/requests"
    COMPONENT   superbuild)
endif ()

include(python.functions)
superbuild_install_superbuild_python()

set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/.plugins")
cmb_add_plugin("${plugins_file}" ${plugins})

install(
  FILES       "${plugins_file}"
  DESTINATION "bin"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "share/cmb/workflows"
  COMPONENT   superbuild)

# ParaView expects this directory to exist.
install(CODE
  "file(MAKE_DIRECTORY \"\$ENV{DESTDIR}\${CMAKE_INSTALL_PREFIX}/lib/paraview-${paraview_version}\")"
  COMPONENT   superbuild)
