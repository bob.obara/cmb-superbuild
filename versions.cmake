superbuild_set_revision(boost
  URL     "https://midas3.kitware.com/midas/download/bitstream/457867/boost_1_60_0.tar.bz2"
  URL_MD5 65a840e1a0b13a558ff19eeb2c4f0cbe)

# Updating to ParaView 5.1.1 - pre-tagged
set(paraview_revision 65d02a7ad1e22e8288bb89fa29491d0ecc65721d)
if (USE_PARAVIEW_master)
  set(paraview_revision origin/master)
endif ()
superbuild_set_revision(paraview
  GIT_REPOSITORY "https://gitlab.kitware.com/paraview/paraview.git"
  GIT_TAG        "${paraview_revision}")

superbuild_set_revision(shiboken
  GIT_REPOSITORY "https://github.com/OpenGeoscience/shiboken.git"
  GIT_TAG        origin/smtk-head)

superbuild_set_external_source(smtk
  "https://gitlab.kitware.com/cmb/smtk.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_external_source(cmb
  "https://gitlab.kitware.com/cmb/cmb.git" "origin/master"
  "/dev/null"
  "/dev/null")

superbuild_set_revision(cmbworkflows
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/simulation-workflows.git"
  GIT_TAG        origin/master)

superbuild_set_revision(vxl
  URL     "https://github.com/judajake/vxl/archive/44433e4.zip"
  URL_MD5 f888f4527f0c3b2deeb3b270bf022155)

# Use the tweaked cmake build of zeromq
superbuild_set_revision(zeromq
  GIT_REPOSITORY "https://github.com/robertmaynard/zeromq4-x.git"
  GIT_TAG        origin/master)

# Use master of remus to get fixes for cmb v4
superbuild_set_revision(remus
  GIT_REPOSITORY "https://gitlab.kitware.com/cmb/remus.git"
  GIT_TAG        origin/master)

superbuild_set_revision(kml
  URL     "http://vtk.org/files/support/libkml_fa6c7d8.tar.gz"
  URL_MD5 261b39166b18c2691212ce3495be4e9c)

superbuild_set_revision(gdal
  GIT_REPOSITORY "https://github.com/judajake/gdal-svn.git"
  GIT_TAG        origin/gdal-1.11-cmb)

superbuild_set_revision(moab
  GIT_REPOSITORY "https://bitbucket.org/mathstuf/moab.git"
  GIT_TAG        origin/next)

superbuild_set_revision(triangle
  GIT_REPOSITORY "https://github.com/robertmaynard/triangle.git"
  GIT_TAG        origin/master)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsix
  URL     "https://pypi.python.org/packages/source/s/six/six-1.10.0.tar.gz"
  URL_MD5 34eed507548117b2ab523ab14b2f8b55)

superbuild_set_revision(pythonrequests
  URL     "https://pypi.python.org/packages/source/r/requests/requests-2.9.1.tar.gz"
  URL_MD5 0b7f480d19012ec52bab78292efd976d)

superbuild_set_revision(pythonsetuptools
  URL     "https://pypi.python.org/packages/45/5e/79ca67a0d6f2f42bfdd9e467ef97398d6ad87ee2fa9c8cdf7caf3ddcab1e/setuptools-23.0.0.tar.gz"
  URL_MD5 100a90664040f8ff232fbac02a4c5652)

superbuild_set_revision(pythongirderclient
  URL     "https://pypi.python.org/packages/source/g/girder-client/girder-client-1.1.2.tar.gz"
  URL_MD5 4cd5e0cab41337a41f45453d25193dcf)

superbuild_set_revision(ftgl
  GIT_REPOSITORY "https://github.com/mathstuf/ftgl.git"
  GIT_TAG        origin/create-cmake-config-file)

superbuild_set_revision(oce
  GIT_REPOSITORY "https://github.com/mathstuf/oce.git"
  GIT_TAG        origin/next-x11)

superbuild_set_revision(cgm
  GIT_REPOSITORY "https://bitbucket.org/mathstuf/cgm.git"
  GIT_TAG        origin/update-cmakelists)
